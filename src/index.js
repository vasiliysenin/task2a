import express from 'express';
import cors from 'cors';
// import canonize from './canonize';
// var express = require('express');
const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'Hell World!',
  });
});

// http://localhost:3000/task2A/?a=2&b=82
app.get('/task2A', (req, res) => {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  console.log('сумма=' + sum.toString());
  res.send(sum.toString());
});
// коммит
app.listen(3000, function () {
  console.log('my new Example app listening on port 3000!');
});


//
// import express from 'express';
// import cors from 'cors';
//
// const app = express();
// app.use(cors());
// app.get('/', (req, res) => {
//   res.json({
//     hello: 'JS World',
//   });
// });
//
// app.listen(3000, () => {
//   console.log('Your app listening on port 3000!');
// });
